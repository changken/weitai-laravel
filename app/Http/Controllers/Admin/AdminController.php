<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin.index',[
            'userId' => Auth::id(),
            'userName' => Auth::user()->name,
            'userEmail' => Auth::user()->email,
            'userLevel' => Auth::user()->level,
        ]);
    }

    public function uploadImg(Request $request)
    {
        $path = $request->file('upload')
                        ->store('public/images');

        return response()->json([
            'uploaded' => true,
            'url' => url(Storage::url($path))
        ]);
    }
}
