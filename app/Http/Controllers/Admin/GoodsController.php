<?php

namespace App\Http\Controllers\Admin;

use App\Catalog;
use App\Good;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class GoodsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('goods.index',[
            'rows' => Good::with('user:id,name')
                            ->paginate(5)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create',  Good::class);

        return view('goods.create',[
            'catalogs' => Catalog::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $good = Good::create([
            'user_id' => $request->input('user_id'),
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'price' => $request->input('price'),
            'amount' => $request->input('amount')
        ]);

        //新增文章關聯
        $good->catalogs()->attach($request->input('catalogs'));

        //清除緩存
        Cache::flush();

        return response()->redirectToRoute('goods.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('view', new Good());

        return view('goods.show',[
            'row' => Good::findOrFail($id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $row = Good::findOrFail($id);

        $this->authorize('update', $row);

        $selectCatalogs = $row->catalogs
            ->pluck('id')
            ->all();//回傳分類的陣列

        return view('goods.edit',[
            'row' => $row,
            'selectCatalogs' => $selectCatalogs,
            'catalogs' => Catalog::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $good = Good::findOrFail($id);
        $good->update([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'price' => $request->input('price'),
            'amount' => $request->input('amount')
        ]);

        //更新關聯
        $good->catalogs()->sync($request->input('catalogs'));

        //清除緩存
        Cache::flush();

        return response()->redirectToRoute('goods.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('delete', Good::findOrFail($id));

        Good::destroy($id);

        //清除緩存
        Cache::flush();

        return response()->redirectToRoute('goods.index');
    }
}
