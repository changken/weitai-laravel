<?php

namespace App\Http\Controllers\Admin;

use App\Catalog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Post;
use Illuminate\Support\Facades\Cache;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('posts.index', [
            'rows' => Post::with('user:id,name')
                        ->paginate(5)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Post::class);

        return view('posts.create', [
            'catalogs' => Catalog::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //新增文章
        $post = Post::create([
            'user_id' => $request->input('user_id'),
            'title' => $request->input('title'),
            'content' => $request->input('content')
        ]);
        //更新多對多關聯
        $post->catalogs()->attach($request->input('catalogs'));

        //清除緩存
        Cache::flush();

        return response()->redirectToRoute('posts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('view', new Post());

        return view('posts.show', [
            'row' => Post::findOrFail($id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $row = Post::findOrFail($id);

        $this->authorize('update', $row);//檢查帳號是否有更新的權限

        $selectCatalogs = $row->catalogs
                            ->pluck('id')
                            ->all();//回傳分類的陣列

        return view('posts.edit',[
            'row' => $row,
            'selectCatalogs' => $selectCatalogs,
            'catalogs' => Catalog::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::findOrFail($id);
        $post->update([
            'title' => $request->input('title'),
            'content' => $request->input('content')
        ]);

        //更新關聯
        $post->catalogs()->sync($request->input('catalogs'));

        //清除緩存
        Cache::flush();

        return response()->redirectToRoute('posts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('delete', Post::findOrFail($id));//檢查帳號是否有刪除的權限

        Post::destroy($id);

        //清除緩存
        Cache::flush();

        return response()->redirectToRoute('posts.index');
    }
}
