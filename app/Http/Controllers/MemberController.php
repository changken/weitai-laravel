<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class MemberController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['login', 'loginc']);
    }

    public function index()
    {
        return view('member.index',[
            'user' => Auth::user()
        ]);
    }

    public function login()
    {
        return view('member.login');
    }

    public function loginc(Request $request)
    {
        $v = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $data = $request->only('email', 'password');
        $remember = $request->filled('remember');

        if(Auth::attempt($data, $remember)){
            return redirect()->intended(route('member.index'));
        }else{
            return redirect()->route('member.login');
        }
    }

    public function logout()
    {
        Auth::logout();

        return response()->redirectToRoute('member.login');
    }
}
