<?php

namespace App\Http\Controllers;

use App\Catalog;
use App\Good;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class StoreController extends Controller
{
    public function index(Request $request)
    {
        $perPage = 5; //每頁5筆
        $page = $request->input('page', 1); //目前第幾頁

        $rows = Cache::remember('goods_all', now()->addMinutes(60), function (){
            return Good::with(['user:id,name','catalogs:id,name'])
                        ->orderBy('id', 'desc')
                        ->get();
        });

        //分頁
        $paginate = new LengthAwarePaginator($rows->forPage($page,$perPage)->all(), $rows->count(), $perPage);
        $paginate->withPath('/store');

        return view('store.index',[
            'rows' => $paginate
        ]);
    }

    public function good($id)
    {
        $rows = Cache::remember('goods_all', now()->addMinutes(60), function (){
            return Good::with(['user:id,name','catalogs:id,name'])
                        ->orderBy('id', 'desc')
                        ->get();
        })->firstWhere('id', $id);

        return view('store.good', [
            'row' => $rows
        ]);
    }

    /*public function author($id)
    {
        $user = User::findOrFail($id);

        return view('store.author', [
            'user' => $user,
            'rows' => $user->goods()
                            ->paginate(5)
        ]);
    }*/

    public function catalog($id, Request $request)
    {
        $perPage = 5; //每頁5筆
        $page = $request->input('page', 1); //目前第幾頁

        $catalog = Catalog::findOrFail($id, ['id', 'name']);

        $rows = Cache::remember('goods_catalog_' . $id, now()->addMinutes(60), function () use($catalog){
            return $catalog->goods()
                ->with('user:id,name')
                ->orderBy('id', 'desc')
                ->get();
        });

        //分頁
        $paginate = new LengthAwarePaginator($rows->forPage($page,$perPage)->all(), $rows->count(), $perPage);
        $paginate->withPath("/store/catalog/$id");

        return view('store.catalog',[
            'catalog' => $catalog,
            'rows' => $paginate
        ]);
    }
}
