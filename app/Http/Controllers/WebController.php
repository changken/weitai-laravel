<?php

namespace App\Http\Controllers;

use App\Catalog;
use App\Post;
use App\User;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class WebController extends Controller
{
    public function index(Request $request)
    {
        $perPage = 5; //每頁5筆
        $page = $request->input('page', 1); //目前第幾頁

        $rows = Cache::remember('posts_all', now()->addMinutes(60), function (){
            return Post::with(['user:id,name', 'catalogs:id,name'])
                        ->orderBy('id', 'desc')
                        ->get();
        });

        return view('index',[
            'rows' => new LengthAwarePaginator($rows->forPage($page, $perPage)->all(), $rows->count(), $perPage)
        ]);
    }

    public function post($id)
    {
        $rows = Cache::remember('posts_all', now()->addMinutes(60), function (){
            return Post::with(['user:id,name', 'catalogs:id,name'])
                        ->orderBy('id', 'desc')
                        ->get();
        })->firstWhere('id', $id);

        return view('post',[
            'row' => $rows
        ]);
    }

    public function author($id, Request $request)
    {
        $perPage = 5; //每頁5筆
        $page = $request->input('page', 1); //目前第幾頁

        $user = User::findOrFail($id,['id', 'name']);

        $rows = Cache::remember('posts_all', now()->addMinutes(60), function (){
            return Post::with(['user:id,name', 'catalogs:id,name'])
                ->orderBy('id', 'desc')
                ->get();
        })->where('user_id', $id);

        //分頁
        $paginate = new LengthAwarePaginator($rows->forPage($page, $perPage)->all(), $rows->count(), $perPage);
        $paginate->withPath("/author/$id");

        return view('author',[
            'user' => $user,
            'rows' => $paginate
        ]);
    }

    public function catalog($id, Request $request)
    {
        $perPage = 5; //每頁5筆
        $page = $request->input('page', 1); //目前第幾頁

        $catalog = Catalog::findOrFail($id, ['id', 'name']);

        $rows = Cache::remember('posts_catalog_' . $id, now()->addMinutes(60), function () use($catalog){
            return $catalog->posts()
                ->with('user:id,name')
                ->orderBy('id', 'desc')
                ->get();
        });

        //分頁
        $paginate = new LengthAwarePaginator($rows->forPage($page, $perPage)->all(), $rows->count(), $perPage);
        $paginate->withPath("/catalog/$id");

        return view('catalog',[
            'catalog' => $catalog,
            'rows' => $paginate
        ]);
    }
}
