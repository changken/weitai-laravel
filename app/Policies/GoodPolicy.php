<?php

namespace App\Policies;

use App\User;
use App\Good;
use Illuminate\Auth\Access\HandlesAuthorization;

class GoodPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if (in_array($user->level, ['root','a'])) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the good.
     *
     * @param  \App\User  $user
     * @param  \App\Good  $good
     * @return mixed
     */
    public function view(User $user, Good $good)
    {
        return true;
    }

    /**
     * Determine whether the user can create goods.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the good.
     *
     * @param  \App\User  $user
     * @param  \App\Good  $good
     * @return mixed
     */
    public function update(User $user, Good $good)
    {
        return $user->id == $good->user_id;
    }

    /**
     * Determine whether the user can delete the good.
     *
     * @param  \App\User  $user
     * @param  \App\Good  $good
     * @return mixed
     */
    public function delete(User $user, Good $good)
    {
        return $user->id == $good->user_id;
    }

    /**
     * Determine whether the user can restore the good.
     *
     * @param  \App\User  $user
     * @param  \App\Good  $good
     * @return mixed
     */
    public function restore(User $user, Good $good)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the good.
     *
     * @param  \App\User  $user
     * @param  \App\Good  $good
     * @return mixed
     */
    public function forceDelete(User $user, Good $good)
    {
        //
    }
}
