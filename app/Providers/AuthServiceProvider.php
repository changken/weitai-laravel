<?php

namespace App\Providers;

use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        //'App\Model' => 'App\Policies\ModelPolicy',
        'App\Post' => 'App\Policies\PostPolicy',
        'App\Good' => 'App\Policies\GoodPolicy',
        'App\Catalog' => 'App\Policies\CatalogPolicy'
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::before(function ($user,$ability){
            if(in_array($user->level, ['root', 'a'])){
                return true;
            }
        });

        Gate::define('view-admin', function (User $user){
            return in_array($user->level, ['root', 'a', 'e']);
        });

        Gate::define('manage-users',function (User $user){
            return in_array($user->level, ['root', 'a']);
        });
    }
}
