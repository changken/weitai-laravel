<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogGoodTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catalog_good', function (Blueprint $table) {
            /*$table->increments('id');
            $table->timestamps();*/
            $table->unsignedInteger('catalog_id');
            $table->unsignedInteger('good_id');
            $table->primary(['catalog_id', 'good_id']);
            $table->foreign('catalog_id')
                ->references('id')
                ->on('catalogs')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('good_id')
                ->references('id')
                ->on('goods')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catalog_good');
    }
}
