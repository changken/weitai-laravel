@extends('tpl.main-admin')

@section('title', '控制台')

@section('subtitle', '這是真正的控制台')

@section('customHead')
    <style>
        header.masthead {
            background-image: url({{ asset('img/home-bg.jpg') }});
        }
    </style>
@endsection

@section('content')
    <table class="table table-bordered text-center word-1">
        <thead class="thead-dark">
        <tr>
            <th colspan="2">控制台</th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td>您的編號:</td>
                <td>{{ $userId }}</td>
            </tr>
            <tr>
                <td>您的大名:</td>
                <td>{{ $userName }}</td>
            </tr>
            <tr>
                <td>您的email:</td>
                <td>{{ $userEmail }}</td>
            </tr>
            <tr>
                <td>權限:</td>
                <td>
                    @switch($userLevel)
                        @case('root')
                            <span class="badge badge-dark word-1">最高管理者</span>
                        @break
                        @case('a')
                            <span class="badge badge-success word-1">管理者</span>
                        @break
                        @case('e')
                            <span class="badge badge-warning word-1">編輯</span>
                        @break
                    @endswitch
                </td>
            </tr>
        </tbody>
    </table>
@endsection

@section('customJs')
@endsection
