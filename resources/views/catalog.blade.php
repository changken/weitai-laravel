@extends('tpl.main')

@section('title', '韋泰茶具茶葉茶行-' . $catalog->name)

@section('subtitle', '來看看這一系列的文章!')

@section('customHead')
    <style>
        header.masthead{
            background-image: url({{ asset('img/home-bg.jpg') }});
        }
    </style>
@endsection

@section('content')
    @foreach($rows as $row)
        <div class="post-preview">
            <a href="{{ route('post',['id'=>$row->id]) }}">
                <h2 class="post-title">
                    {{ $row->title }}
                </h2>
                <h3 class="post-subtitle">
                    {{-- mb_substr($row->content, 0, 100, 'UTF-8') --}}
                    {!!  mb_split('@more', $row->content)[0] !!}
                </h3>
            </a>
            <p class="post-meta">Posted by
                {{ $row->user->name }}
                on {{ $row->created_at }}</p>
        </div>
        <hr>
    @endforeach
    <!-- Pager -->
    <div class="clearfix">
        {{ $rows->links() }}
        {{--<a class="btn btn-primary float-right" href="#">Older Posts &rarr;</a>--}}
    </div>
@endsection

@section('customJs')

@endsection
