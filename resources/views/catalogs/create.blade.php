@extends('tpl.main-admin')

@section('title', '新增分類')

@section('subtitle', '來新增一個分類吧!')

@section('customHead')
    <style>
        header.masthead {
            background-image: url({{ asset('img/contact-bg.jpg') }});
        }
    </style>
@endsection

@section('content')
    <form action="{{ route('catalogs.store') }}" method="post">
        @csrf
        <table class="table table-bordered">
            <thead class="thead-dark text-center">
            <tr>
                <th colspan="2">新增分類</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td>名稱:</td>
                    <td><input type="text" name="name" maxlength="20" class="form-control" required></td>
                </tr>
                <tr>
                    <td colspan="2"><button type="submit" class="btn btn-primary">新增</button></td>
                </tr>
            </tbody>
        </table>
    </form>
@endsection

@section('customJs')
@endsection
