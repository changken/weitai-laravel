@extends('tpl.main-admin')

@section('title', '編輯分類')

@section('subtitle', '來編輯一個分類吧!')

@section('customHead')
    <style>
        header.masthead {
            background-image: url({{ asset('img/contact-bg.jpg') }});
        }
    </style>
@endsection

@section('content')
    <form action="{{ route('catalogs.update', ['id' => $row->id]) }}" method="post">
        @csrf
        @method('patch')
        <table class="table table-bordered">
            <thead class="thead-dark text-center">
            <tr>
                <th colspan="2">編輯分類</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td>NO.</td>
                    <td>
                        <input type="text" class="form-control" value="{{ $row->id }}" title="不能改" readonly="readonly">
                    </td>
                </tr>
                <tr>
                    <td>名稱:</td>
                    <td><input type="text" name="name" maxlength="20" class="form-control" value="{{ $row->name }}" required></td>
                </tr>
                <tr>
                    <td>發表於:</td>
                    <td><input type="text" class="form-control" value="{{ $row->created_at }}" title="不能改" readonly="readonly"></td>
                </tr>
                <tr>
                    <td>更新於:</td>
                    <td><input type="text" class="form-control" value="{{ $row->updated_at }}" title="不能改" readonly="readonly"></td>
                </tr>
                <tr>
                    <td colspan="2"><button type="submit" class="btn btn-primary">編輯</button></td>
                </tr>
            </tbody>
        </table>
    </form>
@endsection

@section('customJs')
@endsection
