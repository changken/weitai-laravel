@extends('tpl.main-admin')

@section('title', '分類列表')

@section('subtitle', '這是真正的分類列表')

@section('customHead')
    <style>
        header.masthead {
            background-image: url({{ asset('img/contact-bg.jpg') }});
        }
    </style>
    <script>
        function deleteCheck(name) {
            return confirm('您確定要刪除分類[' + name + ']嗎?');
        }
    </script>
@endsection

@section('content')
    <table class="table table-bordered">
        <thead class="thead-dark">
        <tr>
            <th>No.</th>
            <th>名稱</th>
            <th>新增於</th>
            <th>動作</th>
        </tr>
        </thead>
        <tbody>
        @foreach($rows as $row)
            <tr>
                <td>{{ $row->id }}</td>
                <td>{{ $row->name }}</td>
                <td>{{ $row->created_at }}</td>
                <td>
                    <a href="{{ route('catalogs.show', ['id'=>$row->id]) }}" class="btn btn-info">詳細</a>
                    <a href="{{ route('catalogs.edit', ['id'=>$row->id]) }}" class="btn btn-warning">編輯</a>
                    <form action="{{ route('catalogs.destroy', ['id'=>$row->id]) }}" method="post" style="display: inline;">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger" onclick="return deleteCheck('{{ $row->name }}')">刪除</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $rows->links() }}
@endsection

@section('customJs')
@endsection
