@extends('tpl.main-admin')

@section('title', '顯示分類')

@section('subtitle', '顯示分類')

@section('customHead')
    <style>
        header.masthead {
            background-image: url({{ asset('img/contact-bg.jpg') }});
        }
    </style>
@endsection

@section('content')
    <table class="table table-bordered">
        <thead class="thead-dark">
        <tr>
            <th colspan="2">顯示分類</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>No.</td>
            <td>{{ $row->id }}</td>
        </tr>
        <tr>
            <td>名稱</td>
            <td>{{ $row->name }}</td>
        </tr>
        <tr>
            <td>新增於</td>
            <td>{{ $row->created_at }}</td>
        </tr>
        <tr>
            <td>更新於</td>
            <td>{{ $row->updated_at }}</td>
        </tr>
        <tr>
            <td>文章量</td>
            <td>{{ $posts }}個</td>
        </tr>
        <tr>
            <td>商品量</td>
            <td>{{ $goods }}個</td>
        </tr>
        </tbody>
    </table>
@endsection

@section('customJs')
@endsection
