@extends('tpl.main-admin')

@section('title', '編輯商品')

@section('subtitle', '來編輯一個商品吧!')

@section('customHead')
    <style>
        header.masthead {
            background-image: url({{ asset('img/post-sample-image.jpg') }});
        }
    </style>
@endsection

@section('content')
    <form action="{{ route('goods.update', ['id' => $row->id]) }}" method="post">
        @csrf
        @method('patch')
        <table class="table table-bordered">
            <thead class="thead-dark text-center">
            <tr>
                <th colspan="2">編輯商品</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td>NO.</td>
                    <td>
                        <input type="text" class="form-control" value="{{ $row->id }}" title="不能改" readonly="readonly">
                    </td>
                </tr>
                <tr>
                    <td>上架帳號:</td>
                    <td>
                        <input type="text" class="form-control" value="{{ $row->user->name }}" title="不能改" readonly="readonly">
                    </td>
                </tr>
                <tr>
                    <td>名稱:</td>
                    <td><input type="text" name="name" maxlength="50" class="form-control" value="{{ $row->name }}" required></td>
                </tr>
                <tr>
                    <td>敘述:</td>
                    <td><textarea name="description" class="form-control" rows="3" id="editor" required>{{ $row->description }}</textarea></td>
                </tr>
                <tr>
                    <td>價格:</td>
                    <td><input type="text" name="price" pattern="\d+(.\d+|$)" class="form-control" value="{{ $row->price }}" required></td>
                </tr>
                <tr>
                    <td>數量:</td>
                    <td><input type="text" name="amount" pattern="\d+" class="form-control" value="{{ $row->amount }}" required></td>
                </tr>
                <tr>
                    <td>分類</td>
                    <td>
                        <select name="catalogs[]" multiple required class="form-control">
                            @foreach($catalogs as $catalog)
                                <option value="{{ $catalog->id }}" @if(in_array($catalog->id, $selectCatalogs)) selected @endif>{{ $catalog->name }}</option>
                            @endforeach
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>發表於:</td>
                    <td><input type="text" class="form-control" value="{{ $row->created_at }}" title="不能改" readonly="readonly"></td>
                </tr>
                <tr>
                    <td>更新於:</td>
                    <td><input type="text" class="form-control" value="{{ $row->updated_at }}" title="不能改" readonly="readonly"></td>
                </tr>
                <tr>
                    <td colspan="2"><button type="submit" class="btn btn-primary">編輯</button></td>
                </tr>
            </tbody>
        </table>
    </form>
@endsection

@section('customJs')
    @include('tpl.editor',['editorId'=>'#editor'])
@endsection
