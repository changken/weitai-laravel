@extends('tpl.main-admin')

@section('title', '商品列表')

@section('subtitle', '這是真正的商品列表')

@section('customHead')
    <style>
        header.masthead {
            background-image: url({{ asset('img/post-sample-image.jpg') }});
        }
    </style>
    <script>
        function deleteCheck(name) {
            return confirm('您確定要刪除商品[' + name + ']嗎?');
        }
    </script>
@endsection

@section('content')
    <table class="table table-bordered">
        <thead class="thead-dark">
        <tr>
            <th>No.</th>
            <th>帳號</th>
            <th>名稱</th>
            <th>價格</th>
            <th>數量</th>
            <th>發表於</th>
            <th>動作</th>
        </tr>
        </thead>
        <tbody>
        @foreach($rows as $row)
            <tr>
                <td>{{ $row->id }}</td>
                <td>{{ $row->user->name }}</td>
                <td>{{ $row->name }}</td>
                <td>{{ $row->price }}</td>
                <td>{{ $row->amount }}</td>
                <td>{{ $row->created_at }}</td>
                <td>
                    <a href="{{ route('goods.show', ['id'=>$row->id]) }}" class="btn btn-info">詳細</a>
                    <a href="{{ route('goods.edit', ['id'=>$row->id]) }}" class="btn btn-warning">編輯</a>
                    <form action="{{ route('goods.destroy', ['id'=>$row->id]) }}" method="post" style="display: inline;">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger" onclick="return deleteCheck('{{ $row->name }}')">刪除</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $rows->links() }}
@endsection

@section('customJs')

@endsection
