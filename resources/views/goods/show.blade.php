@extends('tpl.main-admin')

@section('title', '顯示商品')

@section('subtitle', '顯示商品')

@section('customHead')
    <style>
        header.masthead {
            background-image: url({{ asset('img/post-sample-image.jpg') }});
        }
    </style>
@endsection

@section('content')
    <table class="table table-bordered">
        <thead class="thead-dark">
        <tr>
            <th colspan="2">顯示商品</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>No.</td>
            <td>{{ $row->id }}</td>
        </tr>
        <tr>
            <td>上架帳號</td>
            <td>{{ $row->user->name }}</td>
        </tr>
        <tr>
            <td>名稱</td>
            <td>{{ $row->name }}</td>
        </tr>
        <tr>
            <td>敘述</td>
            <td>{!! $row->description !!}</td>
        </tr>
        <tr>
            <td>價格</td>
            <td>NT$ {{ $row->price }}</td>
        </tr>
        <tr>
            <td>數量</td>
            <td>{{ $row->amount }}</td>
        </tr>
        <tr>
            <td>發表於</td>
            <td>{{ $row->created_at }}</td>
        </tr>
        <tr>
            <td>更新於</td>
            <td>{{ $row->updated_at }}</td>
        </tr>
        <tr>
            <td colspan="2">
                @foreach($row->catalogs as $catalog)
                    <span class="badge badge-info word-0">
                        {{ $catalog->name  }}
                    </span>
                @endforeach
            </td>
        </tr>
        </tbody>
    </table>
@endsection

@section('customJs')
@endsection
