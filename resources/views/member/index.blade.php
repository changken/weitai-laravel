@extends('tpl.main')

@section('title', '韋泰茶具茶葉茶行-會員中心')

@section('subtitle', '既然有個帳號，那就來跟我們玩玩吧!')

@section('customHead')
    <style>
        header.masthead{
            background-image: url({{ asset('img/home-bg.jpg') }});
        }
    </style>
@endsection

@section('content')
    <table class="table table-bordered table-striped mx-auto">
        <thead class="thead-dark">
            <tr>
                <th colspan="2">會員資訊</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>大名</td>
                <td>{{ $user->name }}</td>
            </tr>
            <tr>
                <td>Email</td>
                <td>{{ $user->email }}</td>
            </tr>
            <tr>
                <td>等級</td>
                <td>
                    @switch($user->level)
                        @case('root')
                        <span class="badge badge-dark">最高管理者</span>
                        @break
                        @case('a')
                        <span class="badge badge-success">管理者</span>
                        @break
                        @case('e')
                        <span class="badge badge-warning">編輯</span>
                        @break
                        @case('u')
                        <span class="badge badge-info">會員</span>
                    @endswitch
                </td>
            </tr>
            <tr>
                <td>註冊在</td>
                <td>{{ $user->created_at }}</td>
            </tr>
        </tbody>
    </table>
@endsection

@section('customJs')

@endsection
