@extends('tpl.main')

@section('title', '韋泰茶具茶葉茶行-登入')

@section('subtitle', '既然有個帳號，那就來跟我們玩玩吧!')

@section('customHead')
    <style>
        header.masthead {
            background-image: url({{ asset('img/home-bg.jpg') }});
        }
    </style>
@endsection

@section('content')
    <form action="{{ route('member.loginc') }}" method="post">
        @csrf
        <table class="table table-bordered mx-auto">
            <thead class="thead-dark">
            <tr>
                <th colspan="2">登入</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Email</td>
                <td><input type="email" name="email" pattern="^(.+)@(.+)\.[a-z]{2,}$" class="form-control" required></td>
            </tr>
            <tr>
                <td>密碼</td>
                <td><input type="password" name="password" class="form-control" required></td>
            </tr>
            <tr>
                <td>記住我?</td>
                <td><input type="checkbox" name="remember" class="custom-checkbox"></td>
            </tr>
            <tr>
                <td colspan="2">
                    <button type="submit" class="btn btn-primary">登入</button>
                </td>
            </tr>
            </tbody>
        </table>
    </form>
@endsection

@section('customJs')

@endsection
