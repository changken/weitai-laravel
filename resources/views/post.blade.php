@extends('tpl.main')

@section('title', '韋泰茶具茶葉茶行-'.$row->title)

@section('subtitle', '來閱讀文章吧!')

@section('customHead')
    <style>
        header.masthead{
            background-image: url({{ asset('img/home-bg.jpg') }});
        }
    </style>
@endsection

@section('content')
    <p class="word-0">{!! str_replace('@more', '', $row->content) !!}</p>
    <blockquote class="blockquote">
        <p class="meta">Posted by
            <a href="{{ route('author',['id'=>$row->user->id]) }}">{{ $row->user->name }}</a>
            on {{ $row->created_at }}</p>
        @foreach($row->catalogs as $catalog)
            <a href="{{ route('catalog', ['id'=>$catalog->id]) }}">
                <span class="badge badge-dark">
                    {{ $catalog->name }}
                </span>&nbsp;
            </a>
        @endforeach
    </blockquote>
@endsection

@section('customJs')

@endsection
