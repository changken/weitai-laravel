@extends('tpl.main-admin')

@section('title', '發表文章')

@section('subtitle', '來發表一篇文章吧!')

@section('customHead')
    <style>
        header.masthead {
            background-image: url({{ asset('img/about-bg.jpg') }});
        }
    </style>
@endsection

@section('content')
    <form action="{{ route('posts.store') }}" method="post">
        @csrf
        <table class="table table-bordered">
            <thead class="thead-dark text-center">
            <tr>
                <th colspan="2">發表文章</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td>發表會員:</td>
                    <td>
                        <input type="hidden" name="user_id" pattern="\d+" value="{{ \Illuminate\Support\Facades\Auth::id() }}" required>
                        <input type="text" class="form-control" value="{{ \Illuminate\Support\Facades\Auth::user()->name }}" readonly="readonly">
                    </td>
                </tr>
                <tr>
                    <td>標題:</td>
                    <td><input type="text" name="title" maxlength="50" class="form-control" required></td>
                </tr>
                <tr>
                    <td>內容:</td>
                    <td><textarea name="content" class="form-control" rows="3" id="editor"></textarea></td>
                </tr>
                <tr>
                    <td>分類</td>
                    <td>
                        <select name="catalogs[]" multiple required class="form-control">
                            @foreach($catalogs as $catalog)
                                <option value="{{ $catalog->id }}">{{ $catalog->name }}</option>
                            @endforeach
                        </select>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><button type="submit" class="btn btn-primary">發表</button></td>
                </tr>
            </tbody>
        </table>
    </form>
@endsection

@section('customJs')
    @include('tpl.editor',['editorId'=>'#editor'])
@endsection
