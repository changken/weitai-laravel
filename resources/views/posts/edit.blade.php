@extends('tpl.main-admin')

@section('title', '編輯文章')

@section('subtitle', '來編輯一篇文章吧!')

@section('customHead')
    <style>
        header.masthead {
            background-image: url({{ asset('img/about-bg.jpg') }});
        }
    </style>
@endsection

@section('content')
    <form action="{{ route('posts.update', ['id' => $row->id]) }}" method="post">
        @csrf
        @method('patch')
        <table class="table table-bordered">
            <thead class="thead-dark text-center">
            <tr>
                <th colspan="2">編輯文章</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td>NO.</td>
                    <td>
                        <input type="text" class="form-control" value="{{ $row->id }}" title="不能改" readonly="readonly">
                    </td>
                </tr>
                <tr>
                    <td>發表會員:</td>
                    <td>
                        <input type="text" class="form-control" value="{{ $row->user->name }}" title="不能改" readonly="readonly">
                    </td>
                </tr>
                <tr>
                    <td>標題:</td>
                    <td><input type="text" name="title" maxlength="50" class="form-control" value="{{ $row->title }}" required></td>
                </tr>
                <tr>
                    <td>內容:</td>
                    <td><textarea name="content" class="form-control" rows="3"  id="editor" required>{{ $row->content }}</textarea></td>
                </tr>
                <tr>
                    <td>分類</td>
                    <td>
                        <select name="catalogs[]" multiple required class="form-control">
                            @foreach($catalogs as $catalog)
                                <option value="{{ $catalog->id }}" @if(in_array($catalog->id, $selectCatalogs)) selected @endif>{{ $catalog->name }}</option>
                            @endforeach
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>發表於:</td>
                    <td><input type="text" maxlength="50" class="form-control" value="{{ $row->created_at }}" title="不能改" readonly="readonly"></td>
                </tr>
                <tr>
                    <td>更新於:</td>
                    <td><input type="text" maxlength="50" class="form-control" value="{{ $row->updated_at }}" title="不能改" readonly="readonly"></td>
                </tr>
                <tr>
                    <td colspan="2"><button type="submit" class="btn btn-primary">編輯</button></td>
                </tr>
            </tbody>
        </table>
    </form>
@endsection

@section('customJs')
    @include('tpl.editor',['editorId'=>'#editor'])
@endsection
