@extends('tpl.main-admin')

@section('title', '文章列表')

@section('subtitle', '這是真正的文章列表')

@section('customHead')
    <style>
        header.masthead {
            background-image: url({{ asset('img/post-bg.jpg') }});
        }
    </style>
    <script>
        function deleteCheck(name) {
            return confirm('您確定要刪除文章[' + name + ']嗎?');
        }
    </script>
@endsection

@section('content')
    <table class="table table-bordered">
        <thead class="thead-dark">
        <tr>
            <th>No.</th>
            <th>帳號</th>
            <th>標題</th>
            <th>發表於</th>
            <th>動作</th>
        </tr>
        </thead>
        <tbody>
        @foreach($rows as $row)
            <tr>
                <td>{{ $row->id }}</td>
                <td>{{ $row->user->name }}</td>
                <td>{{ $row->title }}</td>
                <td>{{ $row->created_at }}</td>
                <td>
                    <a href="{{ route('posts.show', ['id'=>$row->id]) }}" class="btn btn-info">詳細</a>
                    <a href="{{ route('posts.edit', ['id'=>$row->id]) }}" class="btn btn-warning">編輯</a>
                    <form action="{{ route('posts.destroy', ['id'=>$row->id]) }}" method="post" style="display: inline;">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger" onclick="return deleteCheck('{{ $row->title }}')">刪除</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $rows->links() }}
@endsection

@section('customJs')

@endsection
