@extends('tpl.main-admin')

@section('title', '顯示文章')

@section('subtitle', '顯示文章')

@section('customHead')
    <style>
        header.masthead {
            background-image: url({{ asset('img/post-bg.jpg') }});
        }
    </style>
@endsection

@section('content')
    <table class="table table-bordered">
        <thead class="thead-dark">
        <tr>
            <th colspan="2">顯示文章</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>編號</td>
            <td>{{ $row->id }}</td>
        </tr>
        <tr>
            <td>使用者</td>
            <td>{{ $row->user->name }}</td>
        </tr>
        <tr>
            <td>標題</td>
            <td>{{ $row->title }}</td>
        </tr>
        <tr>
            <td>內容</td>
            <td>{!!  $row->content !!}</td>
        </tr>
        <tr>
            <td>發表於</td>
            <td>{{ $row->created_at }}</td>
        </tr>
        <tr>
            <td>更新於</td>
            <td>{{ $row->updated_at }}</td>
        </tr>
        <tr>
            <td colspan="2">
                @foreach($row->catalogs as $catalog)
                    <span class="badge badge-info word-0">
                        {{ $catalog->name  }}
                    </span>
                @endforeach
            </td>
        </tr>
        </tbody>
    </table>
@endsection

@section('customJs')
@endsection
