@extends('tpl.main')

@section('title', '韋泰茶具茶葉茶行-' . $user->name)

@section('subtitle', '來看看這一位作者所上架的商品!')

@section('customHead')
    <style>
        header.masthead{
            background-image: url({{ asset('img/home-bg.jpg') }});
        }
    </style>
@endsection

@section('content')
    @foreach($rows as $row)
        <div class="post-preview">
            <a href="{{ route('store.good',['id' => $row->id]) }}">
                <h2 class="post-title">
                    {{ $row->name }}
                </h2>
                <h3 class="post-subtitle">
                    NT$ {{ $row->price }} 元
                </h3>
            </a>
            <p class="post-meta">Posted by
                {{ $user->name }}
                on {{ $row->created_at }}</p>
        </div>
        <hr>
    @endforeach
    <!-- Pager -->
    <div class="clearfix">
        {{ $rows->links() }}
        {{--<a class="btn btn-primary float-right" href="#">Older Posts &rarr;</a>--}}
    </div>
@endsection

@section('customJs')

@endsection
