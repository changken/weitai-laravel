@extends('tpl.main')

@section('title', '韋泰茶具茶葉茶行-' . $row->name)

@section('subtitle', '來看看這一樣不錯的商品吧!')

@section('customHead')
    <style>
        header.masthead{
            background-image: url({{ asset('img/home-bg.jpg') }});
        }
    </style>
@endsection

@section('content')
    <p class="word-0">
        商品簡介:&nbsp;{!! $row->description !!}
    </p>
    <blockquote class="blockquote">
        <p class="word-3">
            價格: NT$ {{ $row->price }}元<br>
            數量: {{ $row->amount }} 個
        </p>
        <p class="meta">Posted by
            {{ $row->user->name }}
            on {{ $row->created_at }}</p>
        @foreach($row->catalogs as $catalog)
            <a href="{{ route('store.catalog', ['id' => $catalog->id]) }}">
                <span class="badge badge-dark">
                    {{ $catalog->name }}
                </span>&nbsp;
            </a>
        @endforeach
    </blockquote>
@endsection

@section('customJs')

@endsection
