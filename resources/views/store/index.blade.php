@extends('tpl.main')

@section('title', '韋泰茶具茶葉茶行-商店')

@section('subtitle', '來看看我們賣些什麼吧!')

@section('customHead')
    <style>
        header.masthead{
            background-image: url({{ asset('img/home-bg.jpg') }});
        }
    </style>
@endsection

@section('content')
    @foreach($rows as $row)
    <div class="post-preview">
        <a href="{{ route('store.good',['id' => $row->id]) }}">
            <h2 class="post-title">
                {{ $row->name }}
            </h2>
            <h3 class="post-subtitle">
                NT$ {{ $row->price }} 元
            </h3>
        </a>
        <p class="post-meta">Posted by
            {{ $row->user->name }}
            on {{ $row->created_at }}</p>
    </div>
    <hr>
    @endforeach
    <!-- Pager -->
    <div class="clearfix">
        {{ $rows->links() }}
        {{--<a class="btn btn-primary float-right" href="#">Older Posts &rarr;</a>--}}
    </div>
@endsection

@section('customJs')

@endsection
