<script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
<script>
    ClassicEditor
        .create( document.querySelector( '{{ $editorId }}' ), {
            ckfinder: {
                uploadUrl: '{{ route('admin.upload') }}',
                options:{
                    resourceType: 'Images'
                },
                openerMethod: 'modal'
            },
            image: {
                toolbar: [ 'imageStyle:alignLeft', 'imageStyle:full', 'imageStyle:alignRight', '|', 'imageTextAlternative' ],
                styles: [
                    'full',
                    'alignLeft',
                    'alignRight'
                ]
            }
        })
        /*.then( editor => {
            console.log( editor )
        })*/
        .catch( error => {
            console.error( error );
        });
</script>
