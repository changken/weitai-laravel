<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>@yield('title')</title>

    @include('tpl.head')
    {{--自訂義網頁標頭--}}
    @yield('customHead')

</head>

<body>

<!-- Navigation -->
@include('tpl.nav')

<!-- Page Header -->
<header class="masthead">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <div class="site-heading">
                    <h1>@yield('title')</h1>
                    <span class="subheading">@yield('subtitle')</span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main Content -->
<div class="container">
    <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
            @section('content')
            @show
        </div>
    </div>
</div>

<hr>

<!-- Footer -->
@include('tpl.footer')

@include('tpl.customJs')
{{--自訂義Javascript檔案--}}
@yield('customJs')

</body>

</html>
