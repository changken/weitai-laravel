<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand" href="{{ route('index') }}">韋泰茶具茶葉商行</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.index') }}">控制台</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="postsMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        帳號管理
                    </a>
                    <div class="dropdown-menu" aria-labelledby="postsMenu">
                        <a class="dropdown-item" href="{{ route('users.index') }}">帳號列表</a>
                        <a class="dropdown-item" href="{{ route('users.create') }}">新增帳號</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="postsMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        文章
                    </a>
                    <div class="dropdown-menu" aria-labelledby="postsMenu">
                        <a class="dropdown-item" href="{{ route('posts.index') }}">文章列表</a>
                        <a class="dropdown-item" href="{{ route('posts.create') }}">發表文章</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="catalogsMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        分類
                    </a>
                    <div class="dropdown-menu" aria-labelledby="catalogsMenu">
                        <a class="dropdown-item" href="{{ route('catalogs.index') }}">分類列表</a>
                        <a class="dropdown-item" href="{{ route('catalogs.create') }}">新增分類</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="catalogsMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        商品
                    </a>
                    <div class="dropdown-menu" aria-labelledby="catalogsMenu">
                        <a class="dropdown-item" href="{{ route('goods.index') }}">商品列表</a>
                        <a class="dropdown-item" href="{{ route('goods.create') }}">新增商品</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="frontMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        前台
                    </a>
                    <div class="dropdown-menu" aria-labelledby="frontMenu">
                        <a class="dropdown-item" href="{{ route('member.index') }}">前往前台</a>
                        <a class="dropdown-item" href="{{ route('member.logout') }}">登出</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
