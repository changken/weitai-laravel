<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand" href="{{ route('index') }}">韋泰茶具茶葉商行</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('index') }}">首頁</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('store.index') }}">商店</a>
                </li>
                @auth
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="frontMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            前台列表
                        </a>
                        <div class="dropdown-menu" aria-labelledby="frontMenu">
                            <a class="dropdown-item" href="{{ route('member.index') }}">前往前台</a>
                            <a class="dropdown-item" href="{{ route('member.logout') }}">登出</a>
                        </div>
                    </li>
                    @can('view-admin')
                        <li class="nav-item">
                            <a href="{{ route('admin.index') }}" class="nav-link">後台</a>
                        </li>
                    @endcan
                @else
                    <li class="nav-item">
                        <a href="{{ route('member.login') }}" class="nav-link">登入</a>
                    </li>
                @endauth
            </ul>
        </div>
    </div>
</nav>
