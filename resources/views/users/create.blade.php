@extends('tpl.main-admin')

@section('title', '新增帳號')

@section('subtitle', '來新增一個帳號吧!')

@section('customHead')
    <style>
        header.masthead {
            background-image: url({{ asset('img/home-bg.jpg') }});
        }
    </style>
@endsection

@section('content')
    <form action="{{ route('users.store') }}" method="post">
        @csrf
        <table class="table table-bordered">
            <thead class="thead-dark text-center">
            <tr>
                <th colspan="2">新增帳號</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td>名稱:</td>
                    <td><input type="text" name="name" class="form-control" required></td>
                </tr>
                <tr>
                    <td>Email:</td>
                    <td><input type="email" name="email" pattern="^(.+)@(.+)\.[a-z]{2,}$" class="form-control" required></td>
                </tr>
                <tr>
                    <td>密碼:</td>
                    <td><input type="password" name="password" class="form-control" required></td>
                </tr>
                <tr>
                    <td>等級</td>
                    <td>
                        <select name="level" class="form-control" required>
                            <option value="a">管理員</option>
                            <option value="e">編輯</option>
                            <option value="u">會員</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><button type="submit" class="btn btn-primary">新增</button></td>
                </tr>
            </tbody>
        </table>
    </form>
@endsection

@section('customJs')
@endsection
