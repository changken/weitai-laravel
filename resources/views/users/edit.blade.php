@extends('tpl.main-admin')

@section('title', '編輯帳號')

@section('subtitle', '來編輯一個帳號吧!')

@section('customHead')
    <style>
        header.masthead {
            background-image: url({{ asset('img/home-bg.jpg') }});
        }
    </style>
@endsection

@section('content')
    <form action="{{ route('users.update', ['id' => $row->id]) }}" method="post">
        @csrf
        @method('patch')
        <table class="table table-bordered">
            <thead class="thead-dark text-center">
            <tr>
                <th colspan="2">編輯帳號</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td>NO.</td>
                    <td>
                        <input type="text" class="form-control" value="{{ $row->id }}" title="不能改" readonly="readonly">
                    </td>
                </tr>
                <tr>
                    <td>名稱:</td>
                    <td><input type="text" name="name" class="form-control" value="{{ $row->name }}" required></td>
                </tr>
                <tr>
                    <td>Email:</td>
                    <td><input type="email" name="email" pattern="^(.+)@(.+)\.[a-z]{2,}$" class="form-control" value="{{ $row->email }}" required></td>
                </tr>
                <tr>
                    <td>密碼:</td>
                    <td><input type="password" name="password" class="form-control"></td>
                </tr>
                <tr>
                    <td>等級</td>
                    <td>
                        @if($row->level == "root")
                            <input type="hidden" name="level" value="root">
                            <input type="text" value="最高管理員" class="form-control" title="不能改" readonly>
                        @else
                        <select name="level" class="form-control" required>
                            <option value="a" @if($row->level == "a") selected @endif>管理員</option>
                            <option value="e" @if($row->level == "e") selected @endif>編輯</option>
                            <option value="u" @if($row->level == "u") selected @endif>會員</option>
                        </select>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>發表於:</td>
                    <td><input type="text" class="form-control" value="{{ $row->created_at }}" title="不能改" readonly="readonly"></td>
                </tr>
                <tr>
                    <td>更新於:</td>
                    <td><input type="text" class="form-control" value="{{ $row->updated_at }}" title="不能改" readonly="readonly"></td>
                </tr>
                <tr>
                    <td colspan="2"><button type="submit" class="btn btn-primary">編輯</button></td>
                </tr>
            </tbody>
        </table>
    </form>
@endsection

@section('customJs')
@endsection
