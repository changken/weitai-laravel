@extends('tpl.main-admin')

@section('title', '帳號列表')

@section('subtitle', '這是真正的帳號列表')

@section('customHead')
    <style>
        header.masthead {
            background-image: url({{ asset('img/home-bg.jpg') }});
        }
    </style>
    <script>
        function deleteCheck(name) {
            return confirm('您確定要刪除帳號[' + name + ']嗎?');
        }
    </script>
@endsection

@section('content')
    <table class="table table-bordered">
        <thead class="thead-dark">
        <tr>
            <th>No.</th>
            <th>名稱</th>
            <th>Email</th>
            <th>權限</th>
            <th>新增於</th>
            <th>動作</th>
        </tr>
        </thead>
        <tbody>
        @foreach($rows as $row)
            <tr>
                <td>{{ $row->id }}</td>
                <td>{{ $row->name }}</td>
                <td>{{ $row->email }}</td>
                <td>
                    @switch($row->level)
                        @case('root')
                        <span class="badge badge-dark">最高管理者</span>
                        @break
                        @case('a')
                        <span class="badge badge-success">管理者</span>
                        @break
                        @case('e')
                        <span class="badge badge-warning">編輯</span>
                        @break
                        @case('u')
                        <span class="badge badge-info">會員</span>
                    @endswitch
                </td>
                <td>{{ $row->created_at }}</td>
                <td>
                    <a href="{{ route('users.show', ['id'=>$row->id]) }}" class="btn btn-info">詳細</a>
                    <a href="{{ route('users.edit', ['id'=>$row->id]) }}" class="btn btn-warning">編輯</a>
                    @if($row->level == "root")
                        <button type="button" class="btn btn-danger disabled" disabled title="不能刪">刪除</button>
                    @else
                    <form action="{{ route('users.destroy', ['id'=>$row->id]) }}" method="post" style="display: inline;">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger" onclick="return deleteCheck('{{ $row->name }}')">刪除</button>
                    </form>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $rows->links() }}
@endsection

@section('customJs')
@endsection
