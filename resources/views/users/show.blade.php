@extends('tpl.main-admin')

@section('title', '顯示帳號')

@section('subtitle', '顯示帳號')

@section('customHead')
    <style>
        header.masthead {
            background-image: url({{ asset('img/home-bg.jpg') }});
        }
    </style>
@endsection

@section('content')
    <table class="table table-bordered">
        <thead class="thead-dark">
        <tr>
            <th colspan="2">顯示帳號</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>No.</td>
            <td>{{ $row->id }}</td>
        </tr>
        <tr>
            <td>名稱</td>
            <td>{{ $row->name }}</td>
        </tr>
        <tr>
            <td>Email</td>
            <td>{{ $row->email }}</td>
        </tr>
        <tr>
            <td>Email驗證於</td>
            <td>{{ $row->email_verified_at }}</td>
        </tr>
        <tr>
            <td>等級</td>
            <td>
                @switch($row->level)
                    @case('root')
                    <span class="badge badge-dark">最高管理者</span>
                    @break
                    @case('a')
                    <span class="badge badge-success">管理者</span>
                    @break
                    @case('e')
                    <span class="badge badge-warning">編輯</span>
                    @break
                    @case('u')
                    <span class="badge badge-info">會員</span>
                @endswitch
            </td>
        </tr>
        <tr>
            <td>新增於</td>
            <td>{{ $row->created_at }}</td>
        </tr>
        <tr>
            <td>更新於</td>
            <td>{{ $row->updated_at }}</td>
        </tr>
        </tbody>
    </table>
@endsection

@section('customJs')
@endsection
