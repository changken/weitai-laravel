<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', 'WebController@index')->name('index');
Route::get('/post/{id}', 'WebController@post')->name('post')->where('id', '\d+');
Route::get('/author/{id}', 'WebController@author')->name('author')->where('id', '\d+');
Route::get('/catalog/{id}', 'WebController@catalog')->name('catalog')->where('id', '\d+');

Route::prefix('/store')
    ->name('store.')
    ->group( function (){
        Route::get('/', 'StoreController@index')->name('index');
        Route::get('/good/{id}', 'StoreController@good')->name('good')->where('id', '\d+');
        //Route::get('/author/{id}', 'StoreController@author')->name('author');
        Route::get('/catalog/{id}', 'StoreController@catalog')->name('catalog')->where('id', '\d+');
});

Route::prefix('/member')
    ->name('member.')
    ->group(function (){
        Route::get('/', 'MemberController@index')->name('index');
        Route::get('/login', 'MemberController@login')->name('login');
        Route::post('/loginc', 'MemberController@loginc')->name('loginc');
        Route::get('/logout', 'MemberController@logout')->name('logout');
    });

Route::namespace('Admin')
    ->prefix('/admin')
    ->middleware(['auth', 'can:view-admin'])
    ->group(function (){
    Route::get('/', 'AdminController@index')->name('admin.index');
    Route::post('/upload', 'AdminController@uploadImg')->name('admin.upload');
    Route::resource('/posts', 'PostsController');
    Route::resource('/catalogs', 'CatalogsController');
    Route::resource('/goods', 'GoodsController');
    Route::resource('/users', 'UsersController')->middleware('can:manage-users');
});

//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
