const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/*mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');*/

//編譯模板scss和js檔
if(mix.inProduction()){
    mix.sass('resources/sass/clean-blog.scss', 'public/css/clean-blog.min.css')
        .sass('resources/sass/custom.scss', 'public/css/custom.min.css')
        .js('resources/js/clean-blog.js', 'public/js/clean-blog.min.js')
        .js('resources/js/contact_me.js', 'public/js/contact_me.min.js')
        .js('resources/js/jqBootstrapValidation.js', 'public/js/jqBootstrapValidation.min.js');
}else{
    mix.sass('resources/sass/clean-blog.scss', 'public/css')
        .sass('resources/sass/custom.scss', 'public/css')
        .js('resources/js/clean-blog.js', 'public/js')
        .js('resources/js/contact_me.js', 'public/js')
        .js('resources/js/jqBootstrapValidation.js', 'public/js');
}

//複製相依套件
mix.copy('resources/img', 'public/img')
    .copy('node_modules/jquery/dist/jquery.min.js', 'public/js')
    .copy('node_modules/bootstrap/dist/js/bootstrap.bundle.min.js', 'public/js')
    .copy('node_modules/bootstrap/dist/css/bootstrap.min.css', 'public/css')
    .copy('node_modules/@fortawesome/fontawesome-free/css/all.min.css', 'public/css/fontawesome/css')
    .copy('node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js', 'public/js/ckeditor')
    .copy('node_modules/@ckeditor/ckeditor5-build-classic/build/translations/zh.js', 'public/js/ckeditor/translations');

mix.copyDirectory('node_modules/@fortawesome/fontawesome-free/webfonts', 'public/css/fontawesome/webfonts');
